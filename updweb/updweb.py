#!/usr/bin/python
# -*- coding utf-8 -*-

# Updweb | your story on the webz
# v 0.1

import os
import glob
import sys
import pickle
import re
import time
import config
from story2html import story2html
from PIL import Image
from story2rss import story2rss

htmlfile = []
rssfile = []
website = config.website
storiespath = config.updwebpath + 'stories/'
imagespath = config.updwebpath + 'images/'
thumbspath = config.indexpath + 'images/thumbs/'
updwebpath = config.updwebpath
indexpath = config.indexpath
sitetitle = config.sitetitle
files = []

##------------THUMBNAILS-----------<3
files = os.listdir(imagespath)
try:
    thumbs = os.listdir(thumbspath)
except:
    print "Please make a directory for your thumbnails. In '" + config.indexpath + "' folder make a directory called /images/thumbs/"
    exit()

## Check if new thumbnails should be made
updatefiles = list(set(files) - set(thumbs))
print updatefiles
print "thumbs to update: " + str(len(updatefiles))

## Resize funky thumbnails
for img in updatefiles:
    os.system('chmod +rw '+ imagespath + img)
    image = Image.open(imagespath + img)
    image.thumbnail((400, 400), Image.ANTIALIAS)
    image.save(thumbspath + img, 'JPEG', quality=88)

##-------------HTMLHEAD and HTMLFOOTER-----------<3
with open(updwebpath + "htmlhead", "r") as myfile:
    htmlhead = myfile.read()   
with open(updwebpath + "htmlfooter", "r") as myfile:
    htmlfooter = myfile.read()

## put the beginning of html file from file htmlhead
htmlfile.append(htmlhead)

##-----------------STORIES------------------------Lol
headers = os.listdir(storiespath)
print "there are " + str(len(headers)) + " stories total"

## order stories by date
os.chdir(storiespath)
stories = filter(os.path.isfile, os.listdir(storiespath))
stories.sort(key=os.path.getmtime)
stories.reverse()
print "stories in order from new to oldest " + str(stories)

# append funky stories to htmlfile... aight sir!
for filename in stories:
    htmlfile.append('<div id="' + filename + '" class="story1">')
    htmlfile.extend(story2html(storiespath + filename, 'none.html'))
    htmlfile.append('</div>')

# put the end of html file from htmlfooter
htmlfile.append(htmlfooter)

# feeding the rss to the people
rssfile.append('<?xml version="1.0" encoding="utf-8"?>\n'+'<feed xmlns="http://www.w3.org/2005/Atom">\n'+'\n'+'    <title>' + sitetitle +'</title>\n'+'    <link>' + website + '</link>\n'+'    <description></description>\n')
for filename in stories[:20]:
    rssfile.extend(story2rss(storiespath + filename, 'non'))
rssfile.append('</feed>')

##----------------WRITE THE HTML FILE----------ai, capitain!
print "writing html file..."
f = open(indexpath + 'index.html','wb')
for line in htmlfile:
    f.write(line + '\n')
f.close()

##----------------WRITE THE RSS FILE-------------here we go!
print "writing rss file..."
f = open(indexpath + 'rss.xml','wb')
for line in rssfile:
    f.write(line + '\n')
f.close()

print "congratz, webz up-to-date!"
