#!/usr/bin/env python
# -*- coding: utf-8 -*-

##-------Setup your directories-------

sitetitle = 'Updweb.org | your story on the webz'
website = 'http://updweb.org'
updwebpath = '/srv/www/updweb.org/updweb/'
indexpath = '/srv/www/updweb.org/public_html/'
