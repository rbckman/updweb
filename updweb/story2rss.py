#!/usr/bin/env python
# -*- coding utf-8 -*-

## story to rss
## Oscar Frobergs beautiful work!! (that i've hacked and made a little bit robugly)
## http://oscarfroberg.com

import re
import os.path
import sys
import datetime
import config

def story2rss(infile, outfile):
    from __main__ import (filename, website)
    if os.path.isfile(infile):
        f = open(infile)
        converted = []
        imageintro = '        <image>\n'
        imageoutro = '        </image>\n'
        link = '        <link href="' + website + '#' + filename + '"/>'
        i = 0
        for line in f:
            if i > 0 and line[0] != '!':
                if line[0] != '*':
                    if line[0] != '`':
                        if line[0] != '[':
                            if line[0] != '#':
                                if line[0] != '\n':
                                    converted.append('        <description>'+ line + '</description>\n')           
            image = re.findall(r'\!.*?\!\(.*?\)', line) # matches !something!(imagenumber)
            if image:
                description = re.findall('\!(.*)\!', line)
                imagename = re.findall('\((.*)\)', line)
                imageslist = os.listdir(config.updwebpath + '/images')
                replacement = '        <summary type="html">&lt;img src="'+ config.website + '/images/thumbs/' + imagename[0]+'" /&gt;</summary>'
                line = line.replace(image[0], replacement)
                converted.append(line)
            if i == 0:
                if line[0] == "#":
                    line = line.replace("#", "")
                    converted.append('        <title>' + line + '</title>')
                    converted.append(link)
            i = i + 1

            #print converted

        # POST PROCESSING

        # remove \n newlines
        for i in range(len(converted)):
            converted[i] = re.sub('\n', '', converted[i])
        converted = filter(None, converted)
        rssintro = ['    <entry>']
        rssoutro = ['    </entry>']
        rssready = rssintro + converted + rssoutro

    #uncomment the following lines for standalone use
        #output = open(outfile, "w")
        #for line in rssready:
        #    output.write(line + '\n')
        #output.close()
        f.close()
        return rssready

#for line in story2rss(sys.argv[1], 'rsstest.rss'):
#    print line 


