#!/usr/bin/env python
# -*- coding utf-8 -*-

## Oscar Frobergs beautiful work!! (that i've hacked and made a little bit robugly)
## http://oscarfroberg.com

import re
import os.path
import sys
import datetime
import config

def story2html(infile, outfile):
    if os.path.isfile(infile):
        f = open(infile)
        converted = [] 
        for line in f:
            if line[0] == "`" and line[-2] == "`": # code enclosed in ``
                line = "<pre><code>" + line[1:-2] + "</code></pre>"

            hyperlinks = re.findall(r'\[.*?\]\(.*?\)', line) # matches [something](something)
            if hyperlinks:
                for i in hyperlinks:
                    description = re.search('\[(.*)\]\(', i)
                    description = description.group(1)
                    url = re.search('\]\((.*)\)', i)
                    url = url.group(1)
                    oldstr = "["+description+"]("+url+")"
                    replacement = "<a href='"+url+"'>"+description+"</a>"
                    line = line.replace(oldstr, replacement)

            # for an image link
            #image = re.findall(r'\!.*?\!\(.*?\)', line) # matches !something!(something)
            #if image:
            #    description = re.findall('\!(.*)\!', line)
            #    url = re.findall('\((.*)\)', line)
            #    replacement = '<img src="'+url[0]+'" alt="'+description[0]+'">'
            #    line = line.replace(image[0], replacement)
            #    print line

            # my hacky code for inserting images from my phone with only writing the number of the pic
            # my phone puts the date in the filenames.. i'm too lazy to write the date everytime...
            image = re.findall(r'\!.*?\!\(.*?\)', line) # matches !something!(imagenumber)
            if image:
                description = re.findall('\!(.*)\!', line)
                imagename = re.findall('\((.*)\)', line)
                todaysdate = datetime.datetime.now().strftime('%Y%m%d')
                imageslist = os.listdir(config.updwebpath + '/images')
                imagecount = 0
                # counting how many images taken today, should remove this part from here
                for filename in imageslist:
                    if todaysdate in str(filename):
                        imagecount = imagecount + 1
                if imagename[0].find('.jpg'):
                    imagename[0] = replacement = '<img src="'+ config.website + '/images/thumbs/' + imagename[0]+'" alt="'+description[0]+'">'
                else:
                    imagename[0] = replacement = '<img src="'+ config.website + '/images/thumbs/' + imagename[0]+'.jpg " alt="'+description[0]+'">'
                line = line.replace(image[0], replacement) + '<div class="imgtext"><p>'+ description[0] + '</p></div>'

            if line[0] == "#":
                if line[1] == "#":
                    if line[2] == "#": # h3
                        line = line.replace("###", "<h3>")
                        line = line.replace("\n", "</h3>\n")
                    else: # h2
                        line = line.replace("##", "<h2>")
                        line = line.replace("\n", "</h2>\n")
                else: # h1
                    line = line.replace("#", "<h1>")
                    line = line.replace("\n", "</h1>\n")
        
            if line[0:2] == "* ":
                line = line.replace("* ", "<li>")
                line = line.replace("\n", "</li>\n")

            bolded = re.findall(r'\*\*.*?\*\*', line)
            if bolded:
                for i in bolded:
                    boldwords = re.search(r'\*\*(.*?)\*\*', i)
                    boldwords = boldwords.group(1)
                    oldstr = "**"+boldwords+"**"
                    replacement = "<strong>"+boldwords+"</strong>"
                    line = line.replace(oldstr, replacement)

            italic = re.findall(r'\*.*?\*', line)
            if italic:
                for i in italic:
                    italicwords = re.search(r'\*(.*?)\*', i)
                    italicwords = italicwords.group(1)
                    oldstr = "*"+italicwords+"*"
                    replacement = "<em>"+italicwords+"</em>"
                    line = line.replace(oldstr, replacement)



            converted.append(line)
            #print converted

        # POST PROCESSING
        converted[0] = converted[0].replace("\n", "")

        #
        converted[0] = "<p class='title'>" + converted[0] + "</p>\n"
        # ul tags
        for i in range(len(converted)):
            # search for lists
            litag = "<li>"
            ultag = "<ul>"
            if converted[i][0:4] == litag:
                if len(converted)>i+1: #if the next line isn't EOF
                    if converted[i-1][0:4] in (litag, ultag): # if the above line is a list entry
                        if converted[i+1][0:4] != litag: # if the next line isn't a list entry
                            converted[i] = converted[i].replace("\n", "") # remove newline
                            converted[i] = converted[i]+"</ul>\n" # add <ul></ul> tags
                    elif converted[i+1][0:4] != litag: # if the next line isn't a list entry
                        converted[i] = converted[i].replace("\n", "") # remove newline
                        converted[i] = "<ul>"+converted[i]+"</ul>\n" # add <ul></ul> tags
                    else: # if next line IS a list entry
                        converted[i] = "<ul>"+converted[i] # just add the <ul>
                #else: # this is the last line, it should naturally have at least </ul>
                elif converted[i-1][0:4] in (litag, ultag): # if the above line is a list entry
                    converted[i] = converted[i].replace("\n", "") # remove newline
                    converted[i] = converted[i]+"</ul>\n" # add </ul> tag
                else:
                    converted[i] = converted[i].replace("\n", "") # remove newline
                    converted[i] = "<ul>"+converted[i]+"</ul>\n" # add <ul> and </ul> tags

        # paragraphs
        firstparagraph = True
        for i in range(len(converted)):
            # make the class="firstparahraph"
            #print str(i) + ": " + converted[i],
            if i == 2 and converted[i][0] not in ("\n", "#", "!"):
                    converted[i] = "<p class='firstparagraph'>"+converted[i]+"</p>\n"
                    #print converted[i]
                    firstparagraph = False
            elif converted[i][0] == "!":
                converted[i] = converted[i][1:]
            
            # make the rest of the paragraphs
            if len(converted)>i and not firstparagraph:
                if converted[i][0] not in ("\n") and converted[i][0:2] not in ("<h", "<p", "<u", "<l"):
                    if len(converted)>i+1 and converted[i+1][0] == "\n" and converted[i-1][0] == "\n":
                        # this is a single line paragraph
                        converted[i] = "<p>" + converted[i] + "</p>\n"
                    elif len(converted)>i+1 and converted[i+1][0] != "\n" and converted[i-1][0] == "\n":
                        # first line of a multiline paragraph
                        converted[i] = "<p>" + converted[i] + "\n"
                        for x in range(i, len(converted)):
                            if x == len(converted)-1:
                                #print "------------------EOF----------------"
                                #print converted[x]
                                converted[x] = converted[x] + "</p>\n"
                            if converted[x] == "\n":
                                # last line of a multiline paragraph
                                converted[x-1] = converted[x-1] + "</p>\n"
                                break
                    # if last line is a one line paragraph
                    if i == len(converted)-1 and converted[i-1] == "\n":
                        #print "------------------EOF----------------"
                        #print "IT IS"
                        converted[i] = "<p>" + converted[i] + "</p>\n"

        # remove \n newlines
        for i in range(len(converted)):
            converted[i] = re.sub('\n', '', converted[i])
        converted = filter(None, converted)
                
    #uncomment the following lines for standalone use
        #output = open(outfile, "w")
        #for line in converted:
        #    output.write(line)
        #output.close()
        #f.close()
        return converted

